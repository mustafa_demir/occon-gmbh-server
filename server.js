/* 

Sorry for not using mongo db and required some bu fix. 
Sorry for this. Time is 03:45 AM. right now. I will wake at 06:00 :(

*/

const MessageType = {
    JOIN: 0,
    USER_LIST: 1,
    BID: 2
}

var server = require("http").createServer();
var io = require("socket.io").listen(server)

users = [];
connections = [];
bids = [];

server.listen(process.env.PORT || 3000);
console.log("server running...");

io.sockets.on("connection", function(socket) {

    connections.push(socket);
    console.log("Connected: %s socket connected", connections.length + " socket.id : " + socket.id);

    //Disconnect
    socket.on('disconnected', function(data) {
        users.splice(users.indexOf(socket.username), 1);
        sendUserList();
        connections.splice(connections.indexOf(socket), 1);
        console.log("Disconnected %s sockets connected", connections.length);
    });

    //Send Message
    socket.on('message', function(data) {
        console.log("incoming message >> " + data);
        let dataObj = JSON.parse(data);

        switch (dataObj.messageType) {
            case MessageType.JOIN : 
                
                var usersStatus = checkUser(dataObj.message.username); // is logged
                if(!usersStatus) {
                   var userObject  = {
                        username : dataObj.message.username,
                        areaid : users.length
                    }
                    users.push(userObject);
                }

                let msg = {
                    messageType: MessageType.JOIN,
                    message : {
                        ...userObject,
                        success: true
                    }
                };

                io.sockets.emit('message', {
                    msg,
                });
                sendUserList();
            break;

            case MessageType.BID : 
                bids.push(dataObj.message);
                
                let msgBid = {
                    messageType : MessageType.BID,
                    message : {
                        bidList: bids
                    }
                };
                
                io.sockets.emit('message', {
                    msg: msgBid,
                });
            break;
        }
    });

    function checkUser(usr) {
        let selectedUser = users.filter(u => u.username == usr)[0];
        if(selectedUser) return usr;
        return false;
    }
    function sendUserList() {
        let msg = {
            messageType : MessageType.USER_LIST,
            message : {userList: users}
        }
        io.sockets.emit('message', {
            msg,
        })
    }

})
